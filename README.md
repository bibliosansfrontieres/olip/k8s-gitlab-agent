# Gitlab agent configuration

This repository is responsible of linking Gitlab with our Kubernetes cluster. The link has been done on the Gitlab [Kubernetes clusters](https://gitlab.com/bibliosansfrontieres/olip/k8s-gitlab-agent/-/clusters) page (and only here) where we can find the name of our agent `olip-apps` and the place where the [configuration file](https://gitlab.com/bibliosansfrontieres/olip/k8s-gitlab-agent/-/tree/main/.gitlab/agents/olip-apps) is held. Configuration has been done with the following procedure : [Installing the agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/install/#register-the-agent-with-gitlab)

Current `config.yaml` file is

```
ci_access:   
    groups:  
    - id: "bibliosansfrontieres/olip"
```

You can find details here : [Authorize the agent to access projects in your groups](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#authorize-the-agent-to-access-projects-in-your-groups), in short with `id` we define in which group the agent is going to scan newly created pipeline in order to handle review apps.

# How we use it

For the record, each repository has it own [.gitlab-ci.yml](https://gitlab.com/bibliosansfrontieres/olip/apps/kiwix/-/blob/master/.gitlab-ci.yml) file where we include pipeline definition from [CI repository](https://gitlab.com/bibliosansfrontieres/olip/org/ci/-/tree/develop). What has changed with the use of the agent is the [newly created variable](https://gitlab.com/bibliosansfrontieres/olip/org/ci/-/blob/develop/deploy.definitions.yml#L14) `$KUBE_CONTEXT` that tells where the  configuration for `olip-apps` Kubernetes gitlab agent is stored (currently in this repo : `bibliosansfrontieres/olip/k8s-gitlab-agent:olip-apps`)
